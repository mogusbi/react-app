const paths = require('@mogusbi/gulp-starter/config/paths');
const {src} = require('@mogusbi/gulp-starter/utilities/paths');

module.exports = {
  devtool: 'eval',
  entry: [
    src('ts/index.tsx')
  ],
  module: {
    loaders: [
      {
        include: src('ts'),
        loaders: [
          'babel-loader',
          'ts-loader'
        ],
        test: /\.tsx?$/
      }
    ]
  },
  output: {
    filename: 'app.js',
    path: paths.dest.js,
    publicPath: 'assets/js/'
  },
  resolve: {
    extensions: [
      '.ts',
      '.tsx',
      '.js'
    ],
    modules: [
      src('ts'),
      'node_modules'
    ],
  },
  watch: true
};
