import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {IndexRoute, Route, Router} from 'react-router';
import {AboutComponent, AppComponent, history, HomeComponent, store} from './app';

render(
  <Provider store={store}>
    <Router history={history}>
      <Route component={AppComponent} path="/">
        <IndexRoute component={HomeComponent} />
        <Route component={AboutComponent} path="about" />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('app')
);
