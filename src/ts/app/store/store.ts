import {combineReducers, createStore, Store} from 'redux';
import {routerReducer} from 'react-router-redux';
import {IState} from '../state';

export const store: Store<IState> = createStore<IState>(
  combineReducers({
    routing: routerReducer
  })
);
