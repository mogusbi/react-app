import React from 'react';
import {FooterComponent} from './footer';
import {NavbarComponent} from './navbar';

export class AppComponent extends React.Component {
  public render (): JSX.Element {
    const {children} = this.props;

    return (
      <div>
        <NavbarComponent />

        <main className="container">
          {children}

          <FooterComponent />
        </main>
      </div>
    );
  }
}
