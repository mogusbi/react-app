import React from 'react';

export class HomeComponent extends React.Component {
  public render (): JSX.Element {
    return (
      <div>
        <h1>Welcome</h1>

        <p>This is an example React app to learn how to help me get my head around how it all works.</p>
      </div>
    );
  }
}
