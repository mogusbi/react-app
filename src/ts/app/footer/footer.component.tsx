import React from 'react';

export class FooterComponent extends React.Component {
  public render (): JSX.Element {
    return (
      <footer>
        <hr />

        <p>&copy; {new Date().getFullYear()} - React app</p>
      </footer>
    );
  }
}
