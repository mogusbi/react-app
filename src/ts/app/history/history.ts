import {History} from 'history';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import {store} from '../store';

export const history: History = syncHistoryWithStore(browserHistory, store);
