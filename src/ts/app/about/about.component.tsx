import React from 'react';

export class AboutComponent extends React.Component {
  public render (): JSX.Element {
    return (
      <div>
        <h1>About</h1>

        <p>Your application description page.</p>

        <p>Use this area to provide additional information.</p>
      </div>
    );
  }
}
