export * from './about';
export * from './history';
export * from './home';
export * from './navbar';
export * from './state';
export * from './store';

export * from './app.component';
