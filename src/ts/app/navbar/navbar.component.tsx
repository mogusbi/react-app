import React from 'react';
import {Link} from 'react-router';

export class NavbarComponent extends React.Component {
  public render (): JSX.Element {
    return (
      <div className="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <Link className="navbar-brand" to="/" title="Back to home">React App</Link>

        <div className="collapse navbar-collapse">
          <div className="navbar-nav">
            <Link className="nav-item nav-link" activeClassName="active" to="/about">About</Link>
          </div>
        </div>
      </div>
    );
  }
}
